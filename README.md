# &lt;my-repo&gt;

> A bare minimum custom element starter-kit using [VanillaJS](http://vanilla-js.com/).
>

## Install


```sh
$ npm install
```

## Usage

1. Import polyfill:

    ```html
    <script src="node_modules/@webcomponents/webcomponentsjs/webcomponents-bundle.js"></script>
    ```

2. Import custom element:

    ```html
    <link rel="import" href="my-element.html">
    ```

3. Start using it!

    ```html
    <my-element></my-element>
    ```

## Options

Attribute     | Options     | Default      | Description
---           | ---         | ---          | ---
`foo`         | *string*    | `bar`        | Lorem ipsum dolor.

## Methods

Method        | Parameters   | Returns     | Description
---           | ---          | ---         | ---
`unicorn()`   | None.        | Nothing.    | Magic stuff appears.

## Events

Event         | Description
---           | ---
`onsomething` | Triggers when something happens.

## Development


